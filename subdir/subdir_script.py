from pprint import pprint

print("Executing subdir script.")
out = [i ** i for i in range(100)]
with open("../../project_data/subdir_output.txt", "w") as fh:
    pprint(out, stream=fh)
