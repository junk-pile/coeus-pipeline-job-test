from os import listdir as ls
from pprint import pprint

print("Executing root script.")
out = ls()

with open("../project_data/root_output.txt", "w") as fh:
    pprint(out, stream=fh)
